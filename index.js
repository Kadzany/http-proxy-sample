/**
 * Http proxy sample for downloading mobile SDK
 * Sample url: 
 * https://github.com/jitsi/jitsi/releases/download/Jitsi-2.10/jitsi-no-jre-2.10.5550.dmg
 * https://github.com/jitsi/jitsi/releases/download/Jitsi-2.10/jitsi_2.10.5550-1_amd64.deb
 * https://github.com/jitsi/jitsi/releases/download/5633/jitsi-2.11.5633-x64.exe
 * 
 * Sample usage:
 * http://localhost:2689/dl/mac?ver=1&api_token=bb267313-4a11-4b1d-8bfd-486a96b5094e
 * http://localhost:2689/dl/nix?ver=1&api_token=bb267313-4a11-4b1d-8bfd-486a96b5094e
 * http://localhost:2689/dl/win?ver=1&api_token=bb267313-4a11-4b1d-8bfd-486a96b5094e
 */
const express = require('express')
const http = require('http')
const fs = require('fs')
const { createProxyMiddleware } = require('http-proxy-middleware');
const app = express()

// Local configuration
const port = 2689
const baseUrl = 'http://github.com/jitsi/jitsi/releases/download/'

const showMsg = (err, res) => {
    console.error(err)
    res.send(err)
}

app.get('/dl/:os', (req, res) => {

    if(!req.query['ver'] || !req.query['api_key']){
        showMsg('please specify valid parameters: /dl/:os?ver=&api_key', res)
        return res.end()
    }

    // validates the available versions
    if(req.query['ver'] != '1'){
        showMsg('no such version available', res)
        return res.end()
    }

    // verifies the api_key validity
    if(req.query['api_key'] != 'bb267313-4a11-4b1d-8bfd-486a96b5094e'){
        showMsg('invalid API key', res)
        return res.end()
    }

    var dlPath = ''
    if(req.params['os'] == 'mac'){
        dlPath = '/dlfile/Jitsi-2.10/jitsi-no-jre-2.10.5550.dmg'
    }
    else if(req.params['os'] == 'nix'){
        dlPath = '/dlfile/Jitsi-2.10/jitsi_2.10.5550-1_amd64.deb'
    }
    else if(req.params['os'] == 'win'){
        dlPath = '/dlfile/5633/jitsi-2.11.5633-x64.exe'
    }
    else{
        showMsg(`please specify correct os-es: 'mac', 'nix' or 'win'.`, res)
        return res.end()
    }

    // if success redirect to middleware
    res.redirect(dlPath)
})

app.use('/dlfile', createProxyMiddleware({
    target: baseUrl,
    changeOrigin: true,
    pathRewrite:{
        [`^/dlfile`]: ''
    }
}))

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})
